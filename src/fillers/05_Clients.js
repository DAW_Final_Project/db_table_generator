// Requireds
const dotenv = require("dotenv").config();
const db = require("../helpers/db");
const utils = require("../helpers/utils");
const lists = require("../lists/lists");

exports.Clients = async () => {
  try {
    // Collection
    const Collection = "clients";

    // Data
    const quantity = 25;
    let Data = [];
    for (let i = 0; i < quantity; i++) {
      let element = {};
      element.created_at = utils.generateDate();
      element.name = utils.randomfrom(lists.names);
      element.surnames = `${utils.randomfrom(
        lists.surnames
      )} ${utils.randomfrom(lists.surnames)}`;
      element.document = utils.generateDocument();
      element.phone = `96${(i % 10) + 1}${(i % 10) + 1}${(i % 10) + 1}${
        (i % 10) + 1
      }${(i % 10) + 1}${(i % 10) + 1}${(i % 10) + 1}`;
      element.email = `client${i<9 ? `0${i+1}` : `${i + 1}`}@modfix.ml`;
      element.address = {};
      element.address.address = `Calle ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.address.city = `Ciudad ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.address.postalCode = `46${(i % 10) + 1}${(i % 10) + 1}${
        (i % 10) + 1
      }`;
      Data.push(element);
    }

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
