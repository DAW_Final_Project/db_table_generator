// Requireds
const db = require("../helpers/db");
const utils = require("../helpers/utils");
const lists = require("../lists/lists");

exports.Services = async () => {
  try {
    // Collection
    const Collection = "services";

    // Data
    let clients = await db.reader("clients");
    let products = await db.reader("products");
    let stores = await db.reader("stores");
    let operations = await db.reader("operations");
    let status = await db.reader("status");
    let accesses = await db.reader("accesses");
    const quantity = 50;
    let Data = [];
    for (let i = 0; i < quantity; i++) {
      let element = {};
      element.created_at = utils.generateDate();
      element.description = `Service ${i<9 ? `0${i+1}` : `${i + 1}`}`;;
      element.client_id =
        clients[Math.floor(Math.random() * clients.length)]._id.toString();
      element.store_id =
        stores[Math.floor(Math.random() * stores.length)]._id.toString();
      element.product_id =
        products[Math.floor(Math.random() * products.length)]._id.toString();
      element.operations = [];
      let operationFound = false;
      do {
        let operationsforproduct = [];
        for (let j = 0; j < operations.length; j++) {
          if (operations[j].product_id === element.product_id) {
            operationsforproduct.push(operations[j]._id.toString());
          }
        }
        if (operationsforproduct.length > 0) {
          operationFound = true;
          operationsNumber = Math.floor(
            Math.random() * (operationsforproduct.length - 1) + 1
          );
          for (let k = 0; k < operationsNumber; k++) {
            let operation = {};
            let operation_id =
              operationsforproduct[
                Math.floor(Math.random() * operationsforproduct.length)
              ];
            let status_id =
              status[Math.floor(Math.random() * status.length)]._id.toString();
            let user_id = null;
            let started_at = null;
            let finished_at = null;
            let statusreceived_id;
            let statusfinished_id;
            for (let l = 0; l < status.length; l++) {
              if (status[l].code === 1) {
                statusreceived_id = status[l]._id.toString();
              }
              if (status[l].code === 3) {
                statusfinished_id = status[l]._id.toString();
              }
            }
            if (status_id != statusreceived_id) {
              let as;
              do {
                as = accesses[Math.floor(Math.random() * accesses.length)];
                user_id = as.user_id;
              } while (as.username.includes("tech"));
              do {
                started_at = utils.generateDate();
              } while (started_at - element.created_at < 0);
              if (status_id == statusfinished_id) {
                do {
                  finished_at = utils.generateDate();
                } while (finished_at - started_at < 0);
              }
            }
            operation.created_at = Date.now();
            operation.operation_id = operation_id;
            operation.status_id = status_id;
            operation.user_id = user_id;
            operation.started_at = started_at;
            operation.finished_at = finished_at;
            element.operations.push(operation);
          }
        } else {
          element.product_id =
            products[
              Math.floor(Math.random() * products.length)
            ]._id.toString();
        }
      } while (!operationFound);

      Data.push(element);
    }

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
