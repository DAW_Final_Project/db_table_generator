let operationSchema = {
  created_at: Date,
  operation_id: String,
  status_id: String,
  started_at: Date,
  finished_at: Date,
  user_id: String,
};

let addressSchema = {
  address: String,
  city: String,
  postalCode: String,
};

exports.schemas = {
  roles: {
    created_at: Date,
    code: Number,
    role: String,
  },

  stores: {
    created_at: Date,
    name: String,
    address: addressSchema,
    phone: String,
    email: String,
  },

  users: {
    created_at: Date,
    name: String,
    surnames: String,
    document: String,
    email: String,
    phone: String,
    address: addressSchema,
    stores: Array,
  },

  accesses: {
    created_at: Date,
    username: String,
    password: String,
    role_id: String,
    user_id: String,
    token: String,
  },

  clients: {
    created_at: Date,
    name: String,
    surnames: String,
    document: String,
    phone: String,
    email: String,
    address: addressSchema
  },

  productsCategories: {
    created_at: Date,
    category: String,
  },

  productsTrademarks: {
    created_at: Date,
    trademark: String,
  },

  products: {
    created_at: Date,
    alias: String,
    model: String,
    trademark_id: String,
    category_id: String,
  },

  msaterialsCategories: {
    created_at: Date,
    category: String,
  },

  materials: {
    created_at: Date,
    material: String,
    category_id: String,
  },

  status: {
    created_at: Date,
    code: Number,
    status: String,
  },

  operations: {
    created_at: Date,
    alias: String,
    description: String,
    product_id: String,
    price: Number,
  },

  services: {
    created_at: Date,
    description: String,
    client_id: String,
    product_id: String,
    store_id: String,
    operations: [operationSchema],
  },
};
