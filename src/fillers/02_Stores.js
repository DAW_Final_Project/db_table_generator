// Requireds
const dotenv = require("dotenv").config();
const db = require("../helpers/db");
const utils = require("../helpers/utils");

exports.Stores = async () => {
  try {
    // Collection
    const Collection = "stores";

    // Data
    const quantity = 5;
    let Data = [];
    for (let i = 0; i < quantity; i++) {
      let element = {};
      element.created_at = utils.generateDate();
      element.name = `Modfix nº${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.address = {};
      element.address.address = `Calle ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.address.city = `Ciudad ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.address.postalCode = `46${i % 10}${i % 10}${i % 10}`;
      element.phone = `96${i % 10}${i % 10}${i % 10}${i % 10}${i % 10}${
        i % 10
      }${i % 10}`;
      element.email = `store${i<9 ? `0${i+1}` : `${i + 1}`}@modfix.ml`;
      Data.push(element);
    }

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
