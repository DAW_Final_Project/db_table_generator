// Requireds
const db = require("../helpers/db");
const utils = require("../helpers/utils");
const md5 = require("md5");

exports.Access = async () => {
  try {
    // Collection
    const Collection = "accesses";

    // Data
    let stores = await db.reader("stores");
    let roles = await db.reader("roles");
    let users = await db.reader("users");
    const adminQuantity = stores.length;
    const deskQuantity = stores.length * 2;
    const techQuantity = stores.length * 5;

    let Data = [];
    let rolesObject = {};
    for (let i = 0; i < roles.length; i++) {
      rolesObject[roles[i].role] = roles[i]._id.toString();
    }
    const assignRoles = (index) => {
      let response = [];
      if (index < adminQuantity) {
        response = rolesObject["administrator"];
      } else if (index < adminQuantity + deskQuantity) {
        response = rolesObject["customercarer"];
      } else {
        response = rolesObject["technician"];
      }
      return response;
    };
    const assignUserNames = (index) => {
      let response = [];
      if (index < adminQuantity) {
        response = `admin${index + 1}`;
      } else if (index < adminQuantity + deskQuantity) {
        response = `desk${index - adminQuantity + 1}`;
      } else {
        response = `tech${index - adminQuantity - deskQuantity + 1}`;
      }
      return response;
    };
    for (let i = 0; i < adminQuantity + deskQuantity + techQuantity; i++) {
      let element = {};
      element.created_at = utils.generateDate();
      element.username = assignUserNames(i);
      element.password = md5("123456");
      element.role_id = assignRoles(i);
      element.user_id = users[i]._id.toString();
      element.token = "";
      Data.push(element);
    }

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
