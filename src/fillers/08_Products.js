// Requireds
const db = require("../helpers/db");
const utils = require("../helpers/utils");

exports.Products = async () => {
  try {
    // Collection
    const Collection = "products";

    // Data
    let productsCategories = await db.reader("productsCategories");
    let productsTrademarks = await db.reader("productsTrademarks");
    const quantity = 25;
    let Data = [];
    for (let i = 0; i < quantity; i++) {
      let element = {};
      element.created_at = utils.generateDate();
      element.alias = `Product ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.model = `Model ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.trademark_id =
        productsTrademarks[
          Math.floor(Math.random() * productsTrademarks.length)
        ]._id.toString();
      element.category_id =
        productsCategories[
          Math.floor(Math.random() * productsCategories.length)
        ]._id.toString();
      Data.push(element);
    }

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
