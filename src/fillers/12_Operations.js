// Requireds
const db = require("../helpers/db");
const utils = require("../helpers/utils");

exports.Operations = async () => {
  try {
    // Collection
    const Collection = "operations";

    // Data
    let products = await db.reader("products");
    const quantity = 35;
    let Data = [];
    for (let i = 0; i < quantity; i++) {
      let element = {};
      element.created_at = utils.generateDate();
      element.alias = `Operation ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.description = `Operation ${i<9 ? `0${i+1}` : `${i + 1}`} Description`;
      element.product_id =
        products[Math.floor(Math.random() * products.length)]._id.toString();
      element.price = Math.floor(Math.random() * 301);
      Data.push(element);
    }

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
