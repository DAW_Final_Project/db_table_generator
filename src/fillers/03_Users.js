// Requireds
const db = require("../helpers/db");
const utils = require("../helpers/utils");
const lists = require("../lists/lists");

exports.Users = async () => {
  try {
    // Collection
    const Collection = "users";

    // Data
    let stores = await db.reader("stores");
    const adminQuantity = stores.length;
    const deskQuantity = stores.length * 2;
    const techQuantity = stores.length * 5;
    let Data = [];
    let adminStores = [];
    for (let i = 0; i < stores.length; i++) {
      adminStores.push(stores[i]._id.toString());
    }
    const assignStores = (index) => {
      let response = [];
      if (index < adminQuantity) {
        response = adminStores;
      } else if (index < adminQuantity + deskQuantity) {
        response.push(
          stores[Math.floor(Math.random() * stores.length)]._id.toString()
        );
      } else {
        let firstIndex = Math.floor(Math.random() * stores.length);
        let secondIndex;
        do {
          secondIndex = Math.floor(Math.random() * stores.length);
        } while (secondIndex == firstIndex);
        response.push(stores[firstIndex]._id.toString());
        response.push(stores[secondIndex]._id.toString());
      }
      return response;
    };
    for (let i = 0; i < adminQuantity + deskQuantity + techQuantity; i++) {
      let element = {};
      element.created_at = utils.generateDate();
      element.name = utils.randomfrom(lists.names);
      element.surnames = `${utils.randomfrom(
        lists.surnames
      )} ${utils.randomfrom(lists.surnames)}`;
      element.document = utils.generateDocument();
      element.email = `user${i<9 ? `0${i+1}` : `${i + 1}`}@modfix.ml`;
      element.phone = `96${(i % 10) + 1}${(i % 10) + 1}${(i % 10) + 1}${
        (i % 10) + 1
      }${(i % 10) + 1}${(i % 10) + 1}${(i % 10) + 1}`;
      element.address = {};
      element.address.address = `Calle ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.address.city = `Ciudad ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.address.postalCode = `46${(i % 10) + 1}${(i % 10) + 1}${
        (i % 10) + 1
      }`;
      element.stores = assignStores(i);
      Data.push(element);
    }

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
