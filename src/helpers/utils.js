exports.randomfrom = (List) => {
  return List[Math.floor(Math.random() * List.length)];
};

exports.generateDocument = () => {
  let number = Math.floor(Math.random() * 100000000);
  const leters = "TRWAGMYFPDXBNJZSQVHLCKET";
  let position = number % 23;
  let leter = leters.substring(position, position + 1);
  const pad = "00000000"  
  let document= `${(pad+number).slice(-pad.length)}${leter}`
  return document
};

exports.generateDate=()=>{
    let date = new Date
    let days =Math.floor(Math.random() * 101)
    date.setDate(date.getDate() - days);
    let response= Date.parse(date)
    return response
}

exports.sleep=(ms)=>{
  return new Promise(resolve => setTimeout(resolve, ms))
}