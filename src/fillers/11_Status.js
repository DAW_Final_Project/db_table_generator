// Requireds
const db = require("../helpers/db");
const utils = require("../helpers/utils");

exports.Status = async () => {
  try {
    // Collection
    const Collection = "status";

    // Data
    let Data = [
      {
        created_at: utils.generateDate(),
        code: 1,
        status: "received",
      },
      {
        created_at: utils.generateDate(),
        code: 2,
        status: "in progres",
      },
      {
        created_at: utils.generateDate(),
        code: 3,
        status: "finished",
      },
    ];

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
