// Requireds
const db = require("./helpers/db");
const { Roles } = require("./fillers/01_Roles");
const { Stores } = require("./fillers/02_Stores");
const { Users } = require("./fillers/03_Users");
const { Access } = require("./fillers/04_Access");
const { Clients } = require("./fillers/05_Clients");
const { ProductsCategories } = require("./fillers/06_ProductsCategories");
const { ProductsTrademarks } = require("./fillers/07_ProductsTrademarks");
const { Products } = require("./fillers/08_Products");
const { MaterialsCategories } = require("./fillers/09_MaterialsCategories");
const { Materials } = require("./fillers/10_Materials");
const { Status } = require("./fillers/11_Status");
const { Operations } = require("./fillers/12_Operations");
const { Services } = require("./fillers/13_Services");

// Main Function
const exec = async () => {
  try {
    await db.connector();
    await db.generator();
    await Roles();
    await Stores();
    await Users();
    await Access();
    await Clients();
    await ProductsCategories();
    await ProductsTrademarks();
    await Products();
    await MaterialsCategories();
    await Materials();
    await Status();
    await Operations();
    await Services();
  } catch (error) {
    console.log(error);
  } finally {
    await db.disconnector();
  }
};

exec();
