// Requireds
const db = require("../helpers/db");
const utils = require("../helpers/utils");

exports.Roles = async () => {
  try {
    // Collection
    const Collection = "roles";

    // Data
    let Data = [
      {
        created_at: utils.generateDate(),
        code: 1,
        role: "administrator",
      },
      {
        created_at: utils.generateDate(),
        code: 2,
        role: "customercarer",
      },
      {
        created_at: utils.generateDate(),
        code: 3,
        role: "technician",
      },
    ];

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
