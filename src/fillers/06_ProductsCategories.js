// Requireds
const db = require("../helpers/db");
const utils = require("../helpers/utils");

exports.ProductsCategories = async () => {
  try {
    // Collection
    const Collection = "productsCategories";

    // Data
    const quantity = 10;
    let Data = [];
    for (let i = 0; i < quantity; i++) {
      let element = {};
      element.created_at = utils.generateDate();
      element.category = `Category ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      Data.push(element);
    }

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
