// Requireds
const dotenv = require("dotenv").config();
const {schemas} = require("../schemas/schemas");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
let collections = [
  "roles", 
  "stores",
  "users",
  "accesses",
  "clients",
  "productsCategories",
  "productsTrademarks",
  "products",
  "materialsCategories",
  "materials",
  "status",
  "operations",
  "services",
];
let models = {};

exports.connector = async () => {
  try {
    console.log("\x1b[33m%s\x1b[0m", `Conecting Database`);
    const uri = `mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_IP}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`;
    await mongoose.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("\x1b[32m%s\x1b[0m", `\tConection Successful\n`);
  } catch (error) {
    console.log(error);
  }
};

exports.disconnector = async () => {
  try {
    console.log("\x1b[33m%s\x1b[0m", `Disconnecting Database`);
    await mongoose.connection.close();
    console.log("\x1b[32m%s\x1b[0m", `\tDisconection Successful\n`);
  } catch (error) {
    console.log(error);
  }
};

exports.generator = async () => {
  try {
    console.log("\x1b[33m%s\x1b[0m", `Generating Models`);
    // Schemas and Models
    for (let i = 0; i < collections.length; i++) {
      let Schema = new mongoose.Schema(schemas[collections[i]]);
      models[collections[i]] = mongoose.model(collections[i], Schema)
      console.log(
        "\x1b[36m%s\x1b[0m",
        `\tModel ${collections[i]} `
      );
    }
    console.log("\x1b[32m%s\x1b[0m", `\tAll Models Generated Successfuly\n`);
  } catch (error) {
    console.log(error);
  }
};

exports.filler = async (collectionName, data) => {
  try {
    console.log("\x1b[33m%s\x1b[0m", `Filling ${collectionName}`);
    let p;
    // Limpiamos la coleccion si existe
    await models[collectionName].deleteMany();

    for (let i = 0; i < data.length; i++) {
      // Creamos el documento
      let Document = new models[collectionName](data[i]);
      await Document.save()
    }
    console.log("\x1b[32m%s\x1b[0m", `\t${collectionName} Filled Successfuly\n`);
  } catch (error) {
    console.log(error);
  }
};

exports.reader = async (collectionName) => {
    try {
      let document = await models[collectionName].find({});
      return document;
    } catch (error) {
      console.log(error);
    }
};
