// Requireds
const db = require("../helpers/db");
const utils = require("../helpers/utils");

exports.Materials = async () => {
  try {
    // Collection
    const Collection = "materials";

    // Data
    let materialsCategories = await db.reader("materialsCategories");
    const quantity = 25;
    let Data = [];
    for (let i = 0; i < quantity; i++) {
      let element = {};
      element.created_at = utils.generateDate();
      element.alias = `Material ${i<9 ? `0${i+1}` : `${i + 1}`}`;
      element.category_id =
        materialsCategories[
          Math.floor(Math.random() * materialsCategories.length)
        ]._id.toString();
      Data.push(element);
    }

    // Filler
    await db.filler(Collection, Data);
  } catch (error) {
    console.log(error);
  }
};
